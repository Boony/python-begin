# Given the side a of a square, find the perimeter P of the square: P = 4·a.
print('Please, enter side of a square:', end=' ')
a = int(input())
p = 4 * a
print('P = {}'.format(p))
