# The sides a and b of a rectangle are given. Find the area S = a·b and the perimeter P = 2·(a + b) of the rectangle.
print('Please, enter a side:', end=' ')
a = int(input())
print('Please, enter b side:', end=' ')
b = int(input())
s = a * b
p = 2 * (a + b)
print('S = {}, P = {}'.format(s, p))
