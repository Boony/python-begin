# Given the side a of a square, find the area S of the square: S = a2.
print('Please, enter a side of square: ', end=' ')
a = int(input())
s = pow(a, 2)
print('S = {}'.format(s))
